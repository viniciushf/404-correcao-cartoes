package br.com.mastertech.imersivo.pagamentos.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.pagamentos.model.Pagamento;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

	List<Pagamento> findAllByCartaoId(Long cartaoId);

}
